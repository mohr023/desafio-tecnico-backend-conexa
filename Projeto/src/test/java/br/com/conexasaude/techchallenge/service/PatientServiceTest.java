package br.com.conexasaude.techchallenge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;

import br.com.conexasaude.techchallenge.model.dto.PatientDTO;
import br.com.conexasaude.techchallenge.model.entity.Appointment;
import br.com.conexasaude.techchallenge.model.entity.Patient;
import br.com.conexasaude.techchallenge.repository.AppointmentRepository;
import br.com.conexasaude.techchallenge.repository.PatientRepository;
import br.com.conexasaude.techchallenge.service.PatientService;

class PatientServiceTest {

	AppointmentRepository appointmentRepository;
	PatientRepository patientRepository;
	
	private PatientService patientService;
	private PatientDTO dto;
	
	@BeforeEach
	void initialize() {
		
		appointmentRepository = mock(AppointmentRepository.class);
		doReturn(new Appointment()).when(appointmentRepository).save(Mockito.any());
		doNothing().when(appointmentRepository).delete(Mockito.any());
		
		patientRepository = mock(PatientRepository.class);
		doReturn(new Patient()).when(patientRepository).save(Mockito.any());
		doNothing().when(patientRepository).delete(Mockito.any());
		
		patientService = new PatientService(patientRepository, appointmentRepository);
		
		dto = new PatientDTO();
		dto.setCpf("12345678910");
		dto.setAge("18");
		dto.setName("Paciente Teste");
		dto.setPhone("11 99999-9999");
	}
	
	@Test
	void testCreatePatientWithoutName() {
		dto.setName(null);
		
		try {
			patientService.createPatient(dto);
		} catch (Exception e) {
			assertEquals("Name is mandatory", e.getMessage());
		}
	}
	
	@Test
	void testCreatePatientWithSameCPF() {
		Patient patient = new Patient();
		patient.setCpf("12345678910");

		doReturn(patient).when(patientRepository).findByCpf("12345678910");
		
		try {
			patientService.createPatient(dto);
		} catch (Exception e) {
			assertEquals("A patient is already registered using that CPF", e.getMessage());
		}
	}
	
	@Test
	void testDeletePatientWithExistingAppointments() {
		Patient patient = new Patient();
		patient.setName("Test Patient");
		patient.setId(1l);
		doReturn(Optional.of(patient)).when(patientRepository).findById(1l);
		
		Appointment appointment1 = new Appointment();
		Appointment appointment2 = new Appointment();
		doReturn(List.of(appointment1, appointment2)).when(appointmentRepository).findByPatient(patient);
		
		try {
			patientService.deletePatient(1l);
			
			verify(patientRepository).delete(patient);
			verify(appointmentRepository, VerificationModeFactory.times(2)).delete(Mockito.any());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
