package br.com.conexasaude.techchallenge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.web.client.HttpServerErrorException;

import br.com.conexasaude.techchallenge.model.dto.AppointmentDTO;
import br.com.conexasaude.techchallenge.model.dto.SymptomDTO;
import br.com.conexasaude.techchallenge.model.entity.ApplicationUser;
import br.com.conexasaude.techchallenge.model.entity.Appointment;
import br.com.conexasaude.techchallenge.model.entity.Medic;
import br.com.conexasaude.techchallenge.model.entity.Patient;
import br.com.conexasaude.techchallenge.repository.ApplicationUserRepository;
import br.com.conexasaude.techchallenge.repository.AppointmentRepository;
import br.com.conexasaude.techchallenge.repository.MedicRepository;
import br.com.conexasaude.techchallenge.repository.PatientRepository;
import br.com.conexasaude.techchallenge.repository.SymptomRepository;

class AppointmentServiceTest {
	
	private AppointmentService appointmentService;
	
	static AppointmentRepository appointmentRepository;
	static ApplicationUserRepository userRepository;
	static PatientRepository patientRepository;

	private String email;

	private AppointmentDTO appointment;

	private ApplicationUser user;
	
	@BeforeEach
	void initialize() {
		appointmentRepository = mock(AppointmentRepository.class);
		doReturn(new Appointment()).when(appointmentRepository).save(Mockito.any());
		
		userRepository = mock(ApplicationUserRepository.class);
		doReturn(new ApplicationUser()).when(userRepository).save(Mockito.any());
		
		patientRepository = mock(PatientRepository.class);
		doReturn(new Patient()).when(patientRepository).save(Mockito.any());
		
		appointmentService = new AppointmentService(appointmentRepository, patientRepository, userRepository, 
				mock(MedicRepository.class), mock(SymptomRepository.class));
		
		email = "test@test.com";
		appointment = new AppointmentDTO();
		appointment.setAppointmentDatetime("2021-12-10 09:00:00");
		appointment.setPatientId("1");
		
		SymptomDTO symptom = new SymptomDTO();
		symptom.setDescription("Fever");
		symptom.setDetails("38 degrees celsius");
		appointment.setSymptoms(List.of(symptom));
		
		user = new ApplicationUser();
		user.setEmail(email);
		user.setMedic(new Medic());
		
		doReturn(user).when(userRepository).findByEmail(email);
	}
	
	@Test
	void testCreateAppointWithoutDate() {
		appointment.setAppointmentDatetime(null);
		
		try {
			appointmentService.createAppointment(appointment, email);
			fail("Should have thrown an exception due to the appointment not having a date");
		} catch (Exception e) {
			assertEquals("Appointment time is mandatory", e.getMessage());
		}
	}
	
	@Test
	void testCreateAppointmentWithoutPatient() {
		appointment.setPatientId(null);
		
		try {
			appointmentService.createAppointment(appointment, email);
			fail("Should have thrown an exception due to the appointment not having a patient");
		} catch (Exception e) {
			assertEquals("Patient's id is mandatory", e.getMessage());
		}
	}
	
	@Test
	void testCreateAppointmentUserNotAMedic() {
		user.setMedic(null);
		
		try {
			appointmentService.createAppointment(appointment, email);
			fail("Should have thrown an exception due to the user not being a medic");
		} catch (HttpServerErrorException e) {
			assertEquals("403 The current user is not a medic", e.getMessage());
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void testCreateAppointmentPatientNotRegistered() {
		doReturn(Optional.empty()).when(patientRepository).findById(Mockito.anyLong());
		
		try {
			appointmentService.createAppointment(appointment, email);
			fail("Should have thrown an exception due to the patient not being registered");
		} catch (Exception e) {
			assertEquals("No patient was found using the supplied id", e.getMessage());
		}
	}
	
	
	@Test
	void testSuccessfullyCreateAppointment () {
		ArgumentCaptor<Appointment> captorAppointment = ArgumentCaptor.forClass(Appointment.class);
		Patient patient = new Patient();
		patient.setId(1l);
		patient.setName("Paciente teste");
		
		doReturn(Optional.of(patient)).when(patientRepository).findById(Mockito.anyLong());
		
		try {
			appointmentService.createAppointment(appointment, email);
			
			verify(appointmentRepository).save(captorAppointment.capture());
			assertEquals(patient, captorAppointment.getValue().getPatient());
		} catch (Exception e) {
			fail();
		}
	}

}
