CREATE TABLE medic (
	id mediumint UNSIGNED not null primary key AUTO_INCREMENT,
	name varchar(50) not null,
	specialty char(50) not null
);

CREATE TABLE application_user (
	id mediumint not null primary key AUTO_INCREMENT,
	email varchar(30) not null,
	password varchar(70) not null,
	cpf varchar(14) not null,
	age smallint not null,
	phone varchar(30) not null,
	medic_id mediumint REFERENCES medic(id) 
);

CREATE TABLE patient(
	id mediumint not null primary key AUTO_INCREMENT,
	name varchar(50) not null,
	cpf varchar(14) not null,
	age int not null,
	email varchar(50) not null,
	phone varchar(30) not null
);

CREATE TABLE appointment (
	id mediumint not null primary key AUTO_INCREMENT,
	appointment_datetime datetime not null,
	medic_id mediumint not null REFERENCES medic(id),
	patient_id mediumint not null REFERENCES patient(id)
);

CREATE TABLE symptom (
	id mediumint not null primary key AUTO_INCREMENT,
	description varchar(50) not null,
	details varchar(150) not null,
	appointment_id mediumint not null REFERENCES appointment(id)
);

INSERT INTO medic (name,specialty) VALUES ("Matheus", "Psicologia");
INSERT INTO medic (name,specialty) VALUES ("Marcelo", "Cardiologia");
INSERT INTO medic (name,specialty) VALUES ("Kegan", "Ortopedia");

-- default password is 'umasenhasegura' encrypted using BCryptPasswordEncoder
INSERT INTO application_user (email,password,cpf, age, phone, medic_id) select "matheus@email.com", "$2a$10$fkfrry3fZceLqTMHYcNS/O9WpFKHMrUqtwr.dYuKrqLBUe0jSfqy.", '99999999988', 30, "(46) 1234-5678", id from medic where name="Matheus";
INSERT INTO application_user (email,password,cpf, age, phone, medic_id) select "marcelo@email.com", "$2a$10$fkfrry3fZceLqTMHYcNS/O9WpFKHMrUqtwr.dYuKrqLBUe0jSfqy.", '88888888877', 30, "(46) 1234-5678", id from medic where name="Marcelo";
INSERT INTO application_user (email,password,cpf, age, phone, medic_id) select "kegan@email.com", "$2a$10$fkfrry3fZceLqTMHYcNS/O9WpFKHMrUqtwr.dYuKrqLBUe0jSfqy.",   '77777777766', 30, "(46) 1234-5678", id from medic where name="Kegan";

INSERT INTO patient (name, cpf, age, email, phone) VALUES ("Jose", "11111111122", 32, "jose@teste.com", "41988887777");
INSERT INTO patient (name, cpf, age, email, phone) VALUES ("Maria", "22222222233", 32, "maria@teste.com","41988887777");
INSERT INTO patient (name, cpf, age, email, phone) VALUES ("Joao", "33333333344", 32, "joao@teste.com","41988887777");