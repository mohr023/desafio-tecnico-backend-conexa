package br.com.conexasaude.techchallenge.model.dto;

import br.com.conexasaude.techchallenge.model.entity.Symptom;

public class SymptomDTO {

	private String description;
	private String details;
	
	public SymptomDTO() {}
	
	public SymptomDTO(Symptom symptom) {
		this.description = symptom.getDescription();
		this.details = symptom.getDetails();
	}

	public Symptom toEntity() {
		return new Symptom(this.description, this.details);
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}
