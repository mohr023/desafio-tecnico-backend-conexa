package br.com.conexasaude.techchallenge.infra.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

@Component
public class JWTLogoutHandler extends SecurityContextLogoutHandler {
	
	@Autowired
	private JWTTokenManager tokenManager;
	
	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		try {
			String token = request.getHeader("Authorization").replace("Bearer ", "");
			tokenManager.removeToken(token);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
