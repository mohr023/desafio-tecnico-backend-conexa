package br.com.conexasaude.techchallenge.infra.auth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import br.com.conexasaude.techchallenge.infra.SpringContextProvider;
import br.com.conexasaude.techchallenge.model.entity.ApplicationUser;
import br.com.conexasaude.techchallenge.repository.ApplicationUserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	private JWTTokenManager tokenManager;
	private ApplicationUserRepository userRepository;
	
	public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
		
		this.userRepository = SpringContextProvider.getBean(ApplicationUserRepository.class);
		this.tokenManager = SpringContextProvider.getBean(JWTTokenManager.class);
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String header = request.getHeader("Authorization");
		
		if (noAuthUrls().stream().anyMatch(url -> url.equals(request.getPathInfo()))) {
			chain.doFilter(request, response);
			return;
		}
		
		String token = header.replace("Bearer ", "").trim();		
		String secret = System.getenv("JWT_SECRET_DESAFIO");
		
		String user = Jwts.parserBuilder()
				.setSigningKey(Keys.hmacShaKeyFor(secret.getBytes()))
				.build()
				.parseClaimsJws(token)
				.getBody().getSubject(); 
		
		if (user != null) {
			ApplicationUser userEntity = userRepository.findByEmail(user);
			
			if (!tokenManager.validToken(token, user) || userEntity == null) {
				response.sendError(403, "Token inválido");
				return;
			}
		}
		
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
		SecurityContextHolder.getContext().setAuthentication(auth);
		chain.doFilter(request, response);
	}
	
	private List<String> noAuthUrls() {
		List<String> urls = new ArrayList<>();
		urls.add("/login");
		urls.add("/signup");
		return urls;
	}

}
