package br.com.conexasaude.techchallenge.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.conexasaude.techchallenge.model.entity.Appointment;
import br.com.conexasaude.techchallenge.model.entity.Medic;
import br.com.conexasaude.techchallenge.model.entity.Patient;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long>{

	List<Appointment> findByPatient(Patient paciente);
	
	@Query("SELECT a from Appointment a WHERE a.medic = ?1 and a.appointmentDatetime > CURRENT_TIMESTAMP")
	List<Appointment> queryFutureAppointments(Medic medic);
	
	List<Appointment> findByMedic(Medic medic);
	
}
