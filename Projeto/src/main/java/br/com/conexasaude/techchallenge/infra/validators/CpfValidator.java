package br.com.conexasaude.techchallenge.infra.validators;

import java.util.regex.Pattern;

public class CpfValidator {

	private static final String cpfRegex = "\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}";
	
	public static boolean validCpf(String cpf) {
		return Pattern.compile(cpfRegex).matcher(cpf).find();
	}
}