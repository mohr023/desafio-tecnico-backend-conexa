package br.com.conexasaude.techchallenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conexasaude.techchallenge.model.entity.ApplicationUser;
import br.com.conexasaude.techchallenge.model.entity.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long>{

	Patient findByName(String nome);
	Patient findByCpf(String cpf);
	
}
