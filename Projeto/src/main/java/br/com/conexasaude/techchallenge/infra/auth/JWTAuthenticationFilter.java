package br.com.conexasaude.techchallenge.infra.auth;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.conexasaude.techchallenge.infra.SpringContextProvider;
import br.com.conexasaude.techchallenge.model.dto.ResponseLoginDTO;
import br.com.conexasaude.techchallenge.model.entity.ApplicationUser;
import br.com.conexasaude.techchallenge.repository.ApplicationUserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private ApplicationUserRepository userRepository;
	private JWTTokenManager jwtTokenManager;
	
	private AuthenticationManager authenticationManager;
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
		
		this.userRepository = SpringContextProvider.getBean(ApplicationUserRepository.class);
		this.jwtTokenManager = SpringContextProvider.getBean(JWTTokenManager.class);
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			ApplicationUser user = new ObjectMapper().readValue(request.getInputStream(), ApplicationUser.class);
			
			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), 
					user.getPassword()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		User authUser = (User) authResult.getPrincipal();
		ApplicationUser user = userRepository.findByEmail(authUser.getUsername());
		
		Date expiryDate = Date.from(LocalDateTime.now().plusHours(2).atZone(ZoneOffset.systemDefault()).toInstant());
		String secret = System.getenv("JWT_SECRET_DESAFIO");
		
		String token = Jwts.builder()
				.setSubject(user.getEmail())
				.setExpiration(expiryDate)
				.signWith(Keys.hmacShaKeyFor(secret.getBytes()))
				.compact();
		
		jwtTokenManager.getValidTokens().put(token, user.getEmail());
		
		ResponseLoginDTO responseDto = new ResponseLoginDTO(token, user.getMedic());
		
		response.getWriter().write(new ObjectMapper().writeValueAsString(responseDto));
	}
	
}
