package br.com.conexasaude.techchallenge.model.dto;

import br.com.conexasaude.techchallenge.model.entity.Patient;

public class PatientDTO {

	private Long id;
	private String name;
	private String email;
	private String cpf;
	private String age;
	private String phone;
	
	public PatientDTO() {}
	
	public PatientDTO(Patient patient) {
		this.id = patient.getId();
		this.name = patient.getName();
		this.email = patient.getEmail();
		this.cpf = patient.getCpf();
		this.age = patient.getAge().toString();
		this.phone = patient.getPhone();
	}
	
	public Patient toEntity() {
		Patient patient = new Patient();
		
		patient.setId(this.id);
		patient.setName(this.name);
		patient.setEmail(this.email);
		patient.setCpf(this.cpf);
		patient.setAge(Integer.parseInt(this.age));
		patient.setPhone(this.phone);
		
		return patient;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
