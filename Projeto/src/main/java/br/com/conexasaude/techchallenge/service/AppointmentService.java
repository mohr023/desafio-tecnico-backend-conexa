package br.com.conexasaude.techchallenge.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import br.com.conexasaude.techchallenge.model.dto.AppointmentDTO;
import br.com.conexasaude.techchallenge.model.dto.SymptomDTO;
import br.com.conexasaude.techchallenge.model.entity.ApplicationUser;
import br.com.conexasaude.techchallenge.model.entity.Appointment;
import br.com.conexasaude.techchallenge.model.entity.Medic;
import br.com.conexasaude.techchallenge.model.entity.Patient;
import br.com.conexasaude.techchallenge.model.entity.Symptom;
import br.com.conexasaude.techchallenge.repository.ApplicationUserRepository;
import br.com.conexasaude.techchallenge.repository.AppointmentRepository;
import br.com.conexasaude.techchallenge.repository.MedicRepository;
import br.com.conexasaude.techchallenge.repository.PatientRepository;
import br.com.conexasaude.techchallenge.repository.SymptomRepository;
import io.jsonwebtoken.lang.Collections;

@Service
public class AppointmentService {

	private AppointmentRepository appointmentRepository;
	private PatientRepository patientRepository;
	private ApplicationUserRepository userRepository;
	private MedicRepository medicRepository;
	private SymptomRepository symptomRepository;

	public AppointmentService(AppointmentRepository appointmentRepository, PatientRepository patientRepository,
			ApplicationUserRepository userRepository, MedicRepository medicRepository, SymptomRepository symptomRepository) {
		this.appointmentRepository = appointmentRepository;
		this.patientRepository = patientRepository;
		this.userRepository = userRepository;
		this.medicRepository = medicRepository;
		this.symptomRepository = symptomRepository;
	}

	@Transactional
	public Long createAppointment(AppointmentDTO dto, String email) throws Exception {
		validateMandatoryFields(dto);
		Date appointmentDatetime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dto.getAppointmentDatetime());
		validateAppointmentDate(appointmentDatetime);
		
		ApplicationUser userEntity = userRepository.findByEmail(email);
		
		if (userEntity.getMedic() == null) throw new HttpServerErrorException(HttpStatus.FORBIDDEN, "The current user is not a medic");
		
		Appointment appointment = new Appointment();
		appointment.setAppointmentDatetime(appointmentDatetime);
		appointment.setMedic(userEntity.getMedic());
		
		Optional<Patient> patient = patientRepository.findById(Long.parseLong(dto.getPatientId()));
		if (!patient.isPresent()) 
			throw new Exception("No patient was found using the supplied id");
			
		appointment.setPatient(patient.get());
		
		appointment = appointmentRepository.save(appointment);
		
		for (SymptomDTO symptomDto : dto.getSymptoms()) {
			Symptom entity = symptomDto.toEntity();
			entity.setAppointment(appointment);
			symptomRepository.save(entity);
		}
		
		return appointment.getId();
	}

	private void validateAppointmentDate(Date appointmentDatetime) throws Exception {
		if (new Date().getTime() > appointmentDatetime.getTime())
			throw new Exception("The appointment date must be a future point in time");
		
	}

	private void validateMandatoryFields(AppointmentDTO dto) throws Exception {
		List<String> errors = new ArrayList<>();
		
		if (dto.getAppointmentDatetime() == null)
			errors.add("Appointment time is mandatory");
		if (dto.getPatientId() == null)
			errors.add("Patient's id is mandatory");
		if (Collections.isEmpty(dto.getSymptoms()))
			errors.add("Provide at least one symptom");
		
		if (!errors.isEmpty())
			throw new Exception(Strings.join(errors, '\n'));
		
	}

	public List<AppointmentDTO> queryAppointmentsByUser(String email) {
		Medic medic = medicRepository.findByEmail(email);
		List<Appointment> appointments = appointmentRepository.queryFutureAppointments(medic);
		return appointments.stream().map(appointment -> new AppointmentDTO(appointment)).collect(Collectors.toList());
	}

	public boolean deleteAppointment(Long id, String email) {
		Medic medic = medicRepository.findByEmail(email);
		List<Appointment> medicAppointments = appointmentRepository.findByMedic(medic);
		
		Optional<Appointment> optAppointment = medicAppointments.stream()
				.filter(appointment -> appointment.getId() == id)
				.findFirst();
		
		if (optAppointment.isPresent()) {
			appointmentRepository.delete(optAppointment.get());
			return true;
		}
		
		return false;
	}
	
}
