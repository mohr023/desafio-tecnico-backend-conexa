package br.com.conexasaude.techchallenge.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import br.com.conexasaude.techchallenge.infra.auth.JWTTokenManager;
import br.com.conexasaude.techchallenge.model.dto.AppointmentDTO;
import br.com.conexasaude.techchallenge.repository.ApplicationUserRepository;
import br.com.conexasaude.techchallenge.service.AppointmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Appointments")
@RequestMapping("appointments")
public class AppointmentController {
		
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private ApplicationUserRepository userRepository;
	
	@Autowired
	private JWTTokenManager tokenManager;
	
	@ApiOperation("Appointment registration")
	@PostMapping
	public ResponseEntity createAppointment(@RequestBody AppointmentDTO dto, HttpServletRequest request) {
		String token = request.getHeader("Authorization").replace("Bearer ", "");
		String email = tokenManager.getValidTokens().get(token);
		
		Long id;
		try {
			id = appointmentService.createAppointment(dto, email);
		} catch (HttpServerErrorException e) {
			return new ResponseEntity<String>(e.getStatusText(), e.getStatusCode());
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Long>(id, HttpStatus.CREATED);
	}
	
	@ApiOperation("List all the appointments for the signed in medic")
	@GetMapping
	public ResponseEntity queryAppointments(HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization").replace("Bearer ", "");
			String email = tokenManager.getValidTokens().get(token);
			
			List<AppointmentDTO> appointments = appointmentService.queryAppointmentsByUser(email);
			
			return new ResponseEntity<List<AppointmentDTO>>(appointments, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation("Appointment registration")
	@DeleteMapping("/{id}")
	public ResponseEntity deleteAppointment(@PathVariable("id") Long id, HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization").replace("Bearer ", "");
			String email = tokenManager.getValidTokens().get(token);
			
			boolean deleted = appointmentService.deleteAppointment(id, email);
			
			if (deleted) {
				return new ResponseEntity<>(HttpStatus.OK);
			}
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch(Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
