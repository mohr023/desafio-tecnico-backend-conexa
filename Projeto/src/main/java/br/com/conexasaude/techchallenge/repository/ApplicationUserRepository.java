package br.com.conexasaude.techchallenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conexasaude.techchallenge.model.entity.ApplicationUser;

@Repository
public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long>{

	ApplicationUser findByEmail(String nome);
	
}
