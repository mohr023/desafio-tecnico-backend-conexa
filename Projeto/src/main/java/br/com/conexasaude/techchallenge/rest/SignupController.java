package br.com.conexasaude.techchallenge.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conexasaude.techchallenge.model.dto.SignupDTO;
import br.com.conexasaude.techchallenge.service.SignupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Signup")
@RequestMapping("signup")
public class SignupController {

	@Autowired
	private SignupService signupService;
	
	@ApiOperation("Appointment registration - for logged in medics")
	@PostMapping
	public ResponseEntity signup(@RequestBody SignupDTO dto) {
		Long id;
		try {
			id = signupService.signup(dto);
		} catch(Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Long>(id, HttpStatus.CREATED);
	}
	
}
