package br.com.conexasaude.techchallenge.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conexasaude.techchallenge.model.dto.PatientDTO;
import br.com.conexasaude.techchallenge.model.entity.Appointment;
import br.com.conexasaude.techchallenge.model.entity.Patient;
import br.com.conexasaude.techchallenge.repository.AppointmentRepository;
import br.com.conexasaude.techchallenge.repository.PatientRepository;

@Service
public class PatientService {

	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired
	private AppointmentRepository appointmentRepository;
	
	public PatientService(PatientRepository patientRepository, AppointmentRepository appointmentRepository) {
		this.patientRepository = patientRepository;
		this.appointmentRepository = appointmentRepository;
	}

	@Transactional
	public Long createPatient(PatientDTO dto) throws Exception {
		validateMandatoryFields(dto);
		validateExistingPatient(dto);
		
		Patient dbPatient = patientRepository.save(dto.toEntity());
		return dbPatient.getId();
	}
	
	public Patient queryPatient(Long id) {
		Optional<Patient> optPatient = patientRepository.findById(id);
		Patient patient = null;
		if (optPatient.isPresent())
			patient = optPatient.get();
		
		return patient;
	}

	private void validateExistingPatient(PatientDTO dto) throws Exception {
		List<String> errors = new ArrayList<>();
		
		Patient patient = patientRepository.findByCpf(dto.getCpf());
		if (patient != null)
			errors.add("A patient is already registered using that CPF");
		
		if (!errors.isEmpty())
			throw new Exception(Strings.join(errors, '\n'));
	}

	private void validateMandatoryFields(PatientDTO dto) throws Exception {
		List<String> errors = new ArrayList<>();
		if (Strings.isEmpty(dto.getName()))
			errors.add("Name is mandatory");
		if(Strings.isEmpty(dto.getCpf()))
			errors.add("CPF is mandatory");
		if(Strings.isEmpty(dto.getAge()))
			errors.add("Age is mandatory");
		if(Strings.isEmpty(dto.getPhone()))
			errors.add("Phone is mandatory");
		
		if (!errors.isEmpty())
			throw new Exception(Strings.join(errors, '\n'));
	}

	@Transactional
	public void updatePatient(PatientDTO dto) throws Exception {
		if (dto.getId() == null)
			throw new Exception("Enter the patient's id");
		
		validateMandatoryFields(dto);
		
		Optional<Patient> patient = patientRepository.findById(dto.getId());
		if (patient.isPresent()) {
			patientRepository.save(dto.toEntity());
		}
	}

	@Transactional
	public boolean deletePatient(Long id) {
		Optional<Patient> optPatient = patientRepository.findById(id);
		
		if (optPatient.isPresent()) {
			List<Appointment> appointments = appointmentRepository.findByPatient(optPatient.get());
			if (appointments != null && !appointments.isEmpty()) {
				appointments.forEach(appointment -> appointmentRepository.delete(appointment));
			}
			
			patientRepository.delete(optPatient.get());
			return true;
		}
				
		return false;
	}

	
	
}
