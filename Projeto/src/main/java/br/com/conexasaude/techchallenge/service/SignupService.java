package br.com.conexasaude.techchallenge.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.conexasaude.techchallenge.infra.validators.CpfValidator;
import br.com.conexasaude.techchallenge.infra.validators.EmailValidator;
import br.com.conexasaude.techchallenge.infra.validators.PhoneValidator;
import br.com.conexasaude.techchallenge.model.dto.SignupDTO;
import br.com.conexasaude.techchallenge.model.entity.ApplicationUser;
import br.com.conexasaude.techchallenge.model.entity.Medic;
import br.com.conexasaude.techchallenge.repository.ApplicationUserRepository;
import br.com.conexasaude.techchallenge.repository.MedicRepository;

@Service
public class SignupService {

	@Autowired
	private ApplicationUserRepository userRepository;
	
	@Autowired
	private MedicRepository medicRepository;
	
	public SignupService(ApplicationUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public Long signup(SignupDTO dto) throws Exception {
		validateMandatoryFields(dto);
		validateFieldFormats(dto);

		Medic medic = new Medic(dto.getName(), dto.getSpecialty());
		ApplicationUser user = new ApplicationUser(dto.getEmail(), new BCryptPasswordEncoder().encode(dto.getPassword()), dto.getCpf(), dto.getAge(), dto.getPhone(), medic);
		
		user = saveNewUser(user, medic);
		
		return user.getId();
	}
	
	@Transactional
	ApplicationUser saveNewUser(ApplicationUser user, Medic medic) {
		medicRepository.save(medic);
		return userRepository.save(user);
	}
	
	private void validateFieldFormats(SignupDTO dto) throws Exception {
		List<String> errors = new ArrayList<>();
		if (!EmailValidator.validEmail(dto.getEmail())) {
			errors.add("Not a valid email");
		}
		if (!CpfValidator.validCpf(dto.getCpf())) {
			errors.add("Not a valid cpf");
		}
		if (!PhoneValidator.validPhone(dto.getPhone())) {
			errors.add("Not a valid phone");
		}
		
		if (!errors.isEmpty())
			throw new Exception(Strings.join(errors, '\n'));
	}

	private void validateMandatoryFields(SignupDTO dto) throws Exception {
		List<String> errors = new ArrayList<>();
		
		if (dto.getEmail() == null)
			errors.add("Email is mandatory");
		if (dto.getPassword() == null)
			errors.add("Password is mandatory");
		if (dto.getConfirmPassword() == null)
			errors.add("Confirm password is mandatory");
		if (dto.getSpecialty() == null)
			errors.add("Specialty is mandatory");
		if (dto.getCpf() == null)
			errors.add("CPF is mandatory");
		if (dto.getAge() == null)
			errors.add("Age is mandatory");
		if (dto.getPhone() == null)
			errors.add("Phone is mandatory");
		
		if (!errors.isEmpty())
			throw new Exception(Strings.join(errors, '\n'));
		
	}
	
}
