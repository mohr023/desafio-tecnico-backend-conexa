package br.com.conexasaude.techchallenge.infra.auth;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.conexasaude.techchallenge.model.entity.ApplicationUser;
import br.com.conexasaude.techchallenge.repository.ApplicationUserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private ApplicationUserRepository userRepository;

	public UserDetailsServiceImpl(ApplicationUserRepository usuarioRepository) {
		this.userRepository = usuarioRepository;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		ApplicationUser user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
	}
	
}
