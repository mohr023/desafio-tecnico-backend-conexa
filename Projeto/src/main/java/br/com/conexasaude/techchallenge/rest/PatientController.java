package br.com.conexasaude.techchallenge.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conexasaude.techchallenge.model.dto.PatientDTO;
import br.com.conexasaude.techchallenge.model.entity.Patient;
import br.com.conexasaude.techchallenge.repository.PatientRepository;
import br.com.conexasaude.techchallenge.service.PatientService;
import io.jsonwebtoken.lang.Collections;

@RestController
@RequestMapping("/patients")
public class PatientController {

	@Autowired
	private PatientService patientService;
	
	@Autowired
	private PatientRepository patientRepository;
	
	@GetMapping(name = "/{id}", produces = "application/json")
	public ResponseEntity queryPatient(@PathParam("id") Long id) {
		if (id == null) return new ResponseEntity<String>("Enter the patient's id", HttpStatus.BAD_REQUEST);
		
		Patient patient = patientService.queryPatient(id);
		
		if (patient != null)
			return new ResponseEntity<Patient>(patient, HttpStatus.OK);
		else
			return new ResponseEntity<String>("Patient not found", HttpStatus.NO_CONTENT);
	}
	
	@GetMapping(name="/")
	public ResponseEntity queryAllPatients() {
		try {
			List<Patient> patients = patientRepository.findAll();
			
			return new ResponseEntity<List<PatientDTO>>(
					patients.stream().map(patient -> new PatientDTO(patient)).collect(Collectors.toList())
					, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<String>("", HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping
	public ResponseEntity createPatient(@RequestBody PatientDTO dto) {
		Long id;
		try {
			id = patientService.createPatient(dto);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Long>(id, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity updatePatient(@RequestBody PatientDTO dto) {
		try {
			patientService.updatePatient(dto);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity deletePatient(@PathVariable("id") Long id) {
		if (id == null) return new ResponseEntity<String>("Enter the patient's id", HttpStatus.BAD_REQUEST);
		
		boolean removeu = patientService.deletePatient(id);
		if (removeu) {
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("Patient not found", HttpStatus.NO_CONTENT);
		}
	}
	
}
