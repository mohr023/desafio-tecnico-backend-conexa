package br.com.conexasaude.techchallenge.infra.auth;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

	private UserDetailsServiceImpl userDetailsService;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private JWTLogoutHandler logoutHandler;
	
	public WebSecurityConfigurer(UserDetailsServiceImpl userDetailsService, 
			BCryptPasswordEncoder bCryptPasswordEncoder, JWTLogoutHandler logoutHandler) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.logoutHandler = logoutHandler;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors()
			.and().csrf()
			.disable()
			.authorizeRequests()
			// Swagger URLs
//			.antMatchers(HttpMethod.GET, "/api/v1/swagger-resources/**").permitAll()
//			.antMatchers(HttpMethod.GET, "/api/v1/swagger-ui/**").permitAll()
			
			.antMatchers(HttpMethod.POST, "/api/v1/signup").permitAll()
			.antMatchers(HttpMethod.POST, "/api/v1/login").permitAll()
			.antMatchers(HttpMethod.POST, "/api/v1/logoff").permitAll()
			.anyRequest().authenticated()
			.and()
			.logout().addLogoutHandler(logoutHandler).permitAll()
			.logoutSuccessHandler((request, response, authentication) -> {
                response.setStatus(HttpServletResponse.SC_OK);
            })
			.and()
			.addFilter(jwtAuthenticationFilter())
			.addFilter(new JWTAuthorizationFilter(authenticationManager()))
			.logout().logoutUrl("/api/v1/logoff").and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
	
	private Filter jwtAuthenticationFilter() throws Exception {
		JWTAuthenticationFilter jwtAuthenticationFilter = new JWTAuthenticationFilter(authenticationManager());
		jwtAuthenticationFilter.setFilterProcessesUrl("/api/v1/login");
		return jwtAuthenticationFilter;
	}

	@Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
	
}
