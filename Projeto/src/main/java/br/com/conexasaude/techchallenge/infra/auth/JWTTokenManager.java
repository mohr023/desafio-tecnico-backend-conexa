package br.com.conexasaude.techchallenge.infra.auth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

@Component
@ApplicationScope
public class JWTTokenManager {

	// Format: token, usuario
	private Map<String, String> validTokens;
	
	public Map<String, String> getValidTokens() {
		if (validTokens == null) {
			validTokens = new HashMap<>();
		}
		
		return validTokens;
	}
	
	public JWTTokenManager() {
		validTokens = new HashMap<>();
	}

	public boolean validToken(String token, String user) {
		return validTokens != null 
				&& validTokens.get(token) != null 
				&& validTokens.get(token).equals(user);
	}

	public void removeToken(String token) {
		
		if (validTokens.containsKey(token))
			validTokens.remove(token);	
	}
	
}
