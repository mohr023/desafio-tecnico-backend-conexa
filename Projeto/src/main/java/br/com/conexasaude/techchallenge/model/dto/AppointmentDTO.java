package br.com.conexasaude.techchallenge.model.dto;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.conexasaude.techchallenge.model.entity.Appointment;

public class AppointmentDTO {

	@JsonProperty("patient_id")
	private String patientId;
	
	@JsonProperty("appointment_datetime")
	private String appointmentDatetime;
	
	@JsonProperty("symptoms")
	private List<SymptomDTO> symptoms;
	
	public AppointmentDTO() {}
	
	public AppointmentDTO(Appointment appointment) {
		this.symptoms = appointment.getSymptoms().stream()
				.map(symptom -> new SymptomDTO(symptom))
				.collect(Collectors.toList());
		this.patientId = String.valueOf(appointment.getPatient().getId());
		this.appointmentDatetime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(appointment.getAppointmentDatetime());
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getAppointmentDatetime() {
		return appointmentDatetime;
	}

	public void setAppointmentDatetime(String appointmentDatetime) {
		this.appointmentDatetime = appointmentDatetime;
	}

	public List<SymptomDTO> getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(List<SymptomDTO> symptoms) {
		this.symptoms = symptoms;
	}

}
