package br.com.conexasaude.techchallenge.infra.validators;

import java.util.regex.Pattern;

public class PhoneValidator {

	private static final String phoneRegex = "\\(\\d{2}\\)\\s?9?\\d{4}-\\d{4}";
	
	public static boolean validPhone(String phone) {
		return Pattern.compile(phoneRegex).matcher(phone).find();
	}
}