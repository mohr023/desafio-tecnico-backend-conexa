package br.com.conexasaude.techchallenge.model.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.conexasaude.techchallenge.model.entity.Medic;

public class ResponseLoginDTO {

	public ResponseLoginDTO(String token, Medic medic) {
		this.token = token;
		this.medic = medic.getName();
		this.specialty = medic.getSpecialty();
		
		this.dailyAppointments = medic.getAppointments().stream()
				.map(appointment -> new AppointmentDTO(appointment))
				.collect(Collectors.toList());
	}

	private String token;
	private String medic;
	private String specialty;
	
	@JsonProperty("daily_appointments")
	private List<AppointmentDTO> dailyAppointments;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMedic() {
		return medic;
	}

	public void setMedic(String medic) {
		this.medic = medic;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public List<AppointmentDTO> getDailyAppointments() {
		return dailyAppointments;
	}

	public void setDailyAppointments(List<AppointmentDTO> dailyAppointments) {
		this.dailyAppointments = dailyAppointments;
	}

}
