package br.com.conexasaude.techchallenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conexasaude.techchallenge.model.entity.Symptom;

@Repository
public interface SymptomRepository extends JpaRepository<Symptom, Long>{

}
