package br.com.conexasaude.techchallenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.conexasaude.techchallenge.model.entity.Medic;

@Repository
public interface MedicRepository extends JpaRepository<Medic, Long>{

	@Query("select a.medic from ApplicationUser a where a.email = ?1")
	Medic findByEmail(String email);
	
}
