package br.com.conexasaude.techchallenge.model.dto;

public class TokenDTO {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
