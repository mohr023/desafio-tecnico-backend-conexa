# Desafio Técnico backend Conexa

# Observações para o avaliador
Conforme apontado pelo Kegan, o projeto foi trabalhado seguindo convenção de codificação 100% em inglês. Sendo assim, os parâmetros dos endpoints também foram feitos em inglês, ficando um pouco diferentes dos informados no README do projeto. Além disso, também foi solicitado que fosse adicionado um atributo "name" para os médicos.

## Pré-requisitos
### Execução

- Docker
- Docker Compose (Para versões mais antigas do docker, é necessário instalá-lo separadamente)

### Construir o projeto e a imagem

- JDK 8
- Maven3

### Testes

- Postman

## Executando o projeto
- Navegue até a pasta "Projeto"
- Execute o comando "mvn clean package"
- Execute o comando "docker build -t conexa-technical-challenge:1.0 ."
 - Se a tag for modificada, será necessário ajustar a tag dentro do arquivo "docker-compose.yml", no serviço "app".
- Execute o comando "docker-compose up"
Com isso o Docker Compose irá executar 3 serviços:

- MySQL - Porta 3306
- Adminer (SGBD web para operações básicas) - Porta 8081
- API - Porta 8080

Para verificar exemplos de uso dos endpoints disponíveis e avaliar seus retornos, importe esta coleção no Postman (clicando em Import no canto esquerdo superior da ferramenta e indo na aba "Link"):
https://www.getpostman.com/collections/2c962e5a52a8c5d4b6e3

Os testes ali incluem um exemplo para cada endpoint e utilizam variáveis do Postman para compartilhar a token retornada pelo Login. Nesse caso, os endpoints que demandam autenticação devem ser executados somente após executar o Login uma vez.

Para viabilizar os testes, foram incluídos registros iniciais na base de dados, os quais podem ser verificados à partir do arquivo "/desafio-tecnico/src/main/resources/db/migration/V1_0__Initial.sql" ou através da interface web do Adminer. Todos os usuários utilizam a mesma senha, então basta trocar o e-mail no endpoint de Login para testar com os outros (marcelo@email.com e kegan@email.com). Foram criados também 3 pacientes, 3 agendamentos e 3 médicos.
